


import NetworkExtension
import OpenVPNAdapter
import UIKit
class connectViewController: UIViewController {
    
    var providerManager: NETunnelProviderManager!
    @IBOutlet weak var connectBtn: UIButton!
   
    @IBAction func connectActionBtn(sender: UIButton) {
        self.loadProviderManager {
            self.configureVPN(serverAddress: "78.47.241.11", username: "george", password: "george")
        }
        
    }
    
    func configureVPN(serverAddress: String, username: String, password: String) {
        guard let configData = self.readFile(path: "server.ovpn") else { return }
        self.providerManager?.loadFromPreferences { error in
            if error == nil {
                let tunnelProtocol = NETunnelProviderProtocol()
                tunnelProtocol.username = username
                tunnelProtocol.serverAddress = serverAddress
                tunnelProtocol.providerBundleIdentifier = "com.i2vpn.georgeTest.openVpn"
                tunnelProtocol.providerConfiguration = ["ovpn": configData, "username": username, "password": password]
                tunnelProtocol.disconnectOnSleep = false
                self.providerManager.protocolConfiguration = tunnelProtocol
                self.providerManager.localizedDescription = "openVpn-I2host"
                self.providerManager.isEnabled = true
                self.providerManager.saveToPreferences(completionHandler: { (error) in
                    if error == nil  {
                        self.providerManager.loadFromPreferences(completionHandler: { (error) in
                            do {
                                try self.providerManager.connection.startVPNTunnel()
                            } catch let error {
                                print(error.localizedDescription)
                            }
                        })
                    }
                })
            }
        }
    }
    
    
    func loadProviderManager(completion:@escaping () -> Void) {
        NETunnelProviderManager.loadAllFromPreferences { (managers, error) in
            if error == nil {
                self.providerManager = managers?.first ?? NETunnelProviderManager()
                completion()
            }
        }
    }
    
    func readFile(path: String) -> Data? {
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let fileURL = documentDirectory.appendingPathComponent(path)
            return try Data(contentsOf: fileURL, options: .uncached)
        }
        catch let error {
            print(error.localizedDescription)
        }
        return nil
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
