//
//  roundButton.swift
//  SOUQ AL SAGGANA
//
//  Created by George Makhoul on 1/14/18.
//  Copyright © 2018 George Makhoul. All rights reserved.
//

import UIKit
@IBDesignable
class roundbutton: UIButton {
    
    @IBInspectable  var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
        
    }
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
}


